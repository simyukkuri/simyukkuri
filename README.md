# SimYukkuri

ゆっくりのシミュレータを開発します.

## ブランチについて

### OSDN版

test, cantreadcode, trunk, polymorphic-yus, files がOSDN版のブランチです.

### したらば版

shitaraba-apache のみがしたらば版のブランチです.

shitaraba-apache はApacheでライセンスされていた頃のしたらば版をまとめたものです.
現在も残っていたものだけまとめているので抜けもあります.

それ以降のツリーも作成していますが, 現在非公開です.
